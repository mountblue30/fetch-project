
/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

// Using promises and the `fetch` library, do the following. 
// If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.
// Usage of the path libary is recommended

// 1. Fetch all the users
// 2. Fetch all the todos
// 3. Use the promise chain and fetch the users first and then the todos.
// 4. Use the promise chain and fetch the users first and then all the details for each user.
// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

const fs = require("fs/promises");
const path = require("path");


function writeIntoFile(fileName, data) {
    return fs.writeFile(path.join(__dirname, `${fileName}.json`), JSON.stringify(data))
        .then(() => {
            console.log(`The result is written into ${fileName}.json`);
        })
        .catch((err) => {
            console.log(err);
        })
}

function appendIntoFile(fileName, data) {
    return fs.appendFile(path.join(__dirname, `${fileName}.json`), JSON.stringify(data))
        .then(() => {
            return data;
        })
        .catch((err) => {
            console.log(err);
        })
}
function fetchAll(api, dataType, writeIntoAFile) {
    return fetch(api)
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw new Error(`${data.status} - ${data.statusText}`)
            }
        })
        .then((data) => {
            if (writeIntoAFile) {
                writeIntoFile(dataType, data);
            } else {
                return data;
            }

        })

        .catch((err) => {
            console.error(err);
        })
}

// 1) Fetch all users:

// fetchAll("https://jsonplaceholder.typicode.com/users", "users",true)

// 2) Fetch all todos

// fetchAll(" https://jsonplaceholder.typicode.com/todos", "todos",true)



//  3) Use the promise chain and fetch the users first and then the todos.
function fetchUserAndTodo() {
    fetchAll("https://jsonplaceholder.typicode.com/users", "users",true)
        .then(() => {
            return fetchAll(" https://jsonplaceholder.typicode.com/todos", "todos",true)
        })
        .catch((err) => {
            console.log(err);
        })
        .finally(() => {
            console.log("The results are written into the respective files.");
        })


}
// fetchUserAndTodo()



//  4) Use the promise chain and fetch the users first and then all the details for each user.

function getAllUserDetails() {
    fetchAll("https://jsonplaceholder.typicode.com/users", "users",false)
        .then((data) => {
            // console.log(data);
            return Promise.allSettled(data.map((user) => {
                return fetch(`https://jsonplaceholder.typicode.com/users?id=${user["id"]}`)
                .then((data) => {
                    if (data.ok) {
                        return data.json();
                    } else {
                        throw new Error(`${data.status} - ${data.statusText}`)
                    }
                })
                    .then((data) => {
                        appendIntoFile("userDetails", data[0])
                            .catch((err) => {
                                console.log(err);
                            })
                    })
            }))
                .then(() => {
                    console.log("The user details are written into the userDetails.json");
                })
                .catch((err) => {
                    console.log(err);
                })
        })
}

getAllUserDetails()

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo


function getTodoUserDetail() {
    fetchAll(" https://jsonplaceholder.typicode.com/todos", "todos",false)
        .then((data) => {
            return fetch(`https://jsonplaceholder.typicode.com/users?id=${data[0]["userId"]}`)
            .then((data) => {
                if (data.ok) {
                    return data.json();
                } else {
                    throw new Error(`${data.status} - ${data.statusText}`)
                }
            })
                .then((data) => {
                    writeIntoFile("todoListUserDetails", data)
                })
                .catch((err) => {
                    console.log(err);
                })
        })

}
// getTodoUserDetail()